# **HOW TO USE**

Provide a Fire Emblem 3DS sprite sheet from Spriters Resource. You can put this in an input/ (or any other named) folder and add input/ to the path you give the converter, or just place it next to sprite_converter.py.

If it is torso-only, and you wish to attach a head with offsets for each frame, create a .txt file that shares a name with your image.

For example, if my image is Myrmidon.png I would create Myrmidon.txt in the desired directory.

Your .txt file will have lines in this style:
`passive1=(X, Y)`

Where "passive" is one of "passive", "left", "right", "down", or "up". 1 can be replaced with 1, 2, 3, or 4.

View example_offset.txt for a complete example.

Where X and Y are offset integers you wish to define. By default, and for most non-mounted sprites, they will be 0, 0 or close to it.

Run `pip install -r requirements.txt`

Run `python sprite_converter.py` or if you are providing a file `python sprite_converter.py path/to/myfile.txt`

Walk through the steps the program lays out. At the end, the output should be written to your outputs folder.

Message kdports on discord if you run into any issues. Feel free to submit MRs as you see fit.

# **INPUT FILE API**

`torso`: Name of image png with sprite's torso

`is_singular`: Only relevant for Echoes sprites. If yes, then there is no separate head sprite to attach. If no, attach the separate head sprite.

`head`: Only relevant if is_singular is no or format type is Fates.

`fates_head`: Is the provided head sprite sheet from Fates? Yes if so, no otherwise. Defaults to no.

`format`: One of Awakening, Fates, or Echoes. Defaults to Awakening.

`darken_torso`: If yes, darken the colors of the output sprite.

`output_folder`: Output folder to write to

`output_filename`: Desired name of created output file (no file extension)