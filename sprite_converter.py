from PIL import Image
import sys
from pathlib import Path
from code.datatypes import FatesInput, FormatType, InputData, Save
from code.awakening_transform import AwakeningTransform
from code.echoes_transform import EchoesTransform
from code.newstyle_transform import NewStyleTransform

def GetFormatFromString(format_string: str) -> FormatType:
    if format_string.upper() == "AWAKENING":
        return FormatType.AWAKENING
    elif format_string.upper() == "FATES":
        return FormatType.FATES
    else:
        return FormatType.ECHOES

def ReadOffsetFile(torso_text_name: str):
    file = open(torso_text_name, "r")
    offset_dict = {}
    lines = file.readlines()
    for line in lines:
        line_split = line.split('=')
        offset_dict[line_split[0].strip()] = eval(line_split[1])
    return offset_dict

def ReadInputFile(config_name: Path):
    file = config_name.open("r")
    offset_dict = {}
    lines = file.readlines()
    for line in lines:
        line_split = line.split('=')
        offset_dict[line_split[0].strip()] = line_split[1].strip()
    return offset_dict

def InputFromCLI():
    print("WARNING: Using CLI input pathway. CLI currently requires all input files to be in the same directory as sprite_converter.py")
    original_image_name = input("Provide the name of the file to open (with file extension): ")
    try:
        original_img = Image.open(open(original_image_name, "rb"))
    except:
        print("Could not open the image. Is it in this directory? Exiting now.")
        exit()

    try:
        torso_text_name = original_image_name.split('.')[0] + ".txt"
        offset = ReadOffsetFile(torso_text_name)
    except:
        print("No associated txt file found. Defaulting to offsets at (0, 0)")
        offset = {}

    format_type = GetFormatFromString(input("Enter Awakening, Fates, or Echoes to use that sprite sheet format: "))

    darken_torso = False

    data = {}

    if format_type == FormatType.ECHOES:
        is_singular = input("Is this just a torso? If so, you may have a .txt file with the same prefix.\nLine 1 should be offset=(0, 0). Replace 0, 0 with values to better place the head on your torso.\nEnter Yes if this is torso only, any other input otherwise: ")
        data["is_singular"] = is_singular
        if data["is_singular"].upper() == "YES":
            print("Mounted heads are NOT IMPLEMENTED")
            data["head"] = input("Provide the name of the head file to open (with file extension): ")
    elif format_type == FormatType.FATES:
        print("Mounted heads are NOT IMPLEMENTED")
        data["head"] = input("Provide the name of the head file to open (with file extension): ")

    finalized_input = InputData(darken_torso, original_img, offset, format_type, data)

    return finalized_input

def InputFromFile() -> InputData:
    file_path = Path(sys.argv[1])
    input_data = ReadInputFile(file_path)
    darken_torso = input_data.get("darken_torso", "NO").upper() == "YES"
    directory = file_path.parent

    try:
        original_img = Image.open(directory.joinpath(input_data["torso"]).open("rb"))
    except:
        print("Could not open the image. Is it in this directory? Exiting now.")
        exit()

    try:
        torso_text_name = input_data["torso"].split('.')[0] + ".txt"
        offset = ReadOffsetFile(directory.joinpath(torso_text_name))
    except:
        print("No associated txt file found. Defaulting to offsets at (0, 0)")
        offset = {}

    format_type = GetFormatFromString(input_data.get("format", "Awakening"))

    input_data["head"] = directory.joinpath(input_data.get("head", ""))
    input_data["fates_head"] = input_data.get("fates_head", "No")

    input = InputData(darken_torso, original_img, offset, format_type, input_data)

    return input

def GetHeadImage(data: dict) -> Image:
    head_image_name = data.get("head", "")
    try:
        head_image = Image.open(head_image_name)
    except:
        print("Could not open the head image. Is it in this directory? Exiting now.")
        exit()
    return head_image

if __name__ == "__main__":
    inputs = None
    if len(sys.argv) == 2:
        inputs = InputFromFile()
    else:
        inputs = InputFromCLI()

    try:
        shadow = Image.open(open("UnitShadow.png", "rb"))
        shadow = shadow.convert("RGBA")
    except:
        print("Could not open UnitShadow.png. It may be missing or inaccessible.")
        exit()

    save_class = Save(inputs.data.get("output_folder", "."), \
                      inputs.data.get("output_filename", ""))

    if inputs.format_type == FormatType.AWAKENING:

        AwakeningTransform(save_class).DoAwakeningTransform(inputs.original_img, shadow, inputs.offset)

    elif inputs.format_type == FormatType.ECHOES:
        is_singular = inputs.data.get("is_singular", "No")
        if is_singular.upper() == "YES":
            EchoesTransform(save_class).DoEchoesTransformFullbody(inputs.original_img, inputs.offset, shadow)
        else:
            head_image = GetHeadImage(inputs.data)
            fates_input = FatesInput(inputs.original_img, inputs.offset, head_image, shadow, fates_head=inputs.data["fates_head"], darken_torso=inputs.darken_torso)
            NewStyleTransform(save_class).DoNewStyleTransformSplit(fates_input)

    elif inputs.format_type == FormatType.FATES:
        head_image = GetHeadImage(inputs.data)
        fates_input = FatesInput(inputs.original_img, inputs.offset, head_image, shadow, fates_torso=True, fates_head=inputs.data["fates_head"], darken_torso=inputs.darken_torso)
        NewStyleTransform(save_class).DoNewStyleTransformSplit(fates_input)