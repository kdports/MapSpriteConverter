from enum import Enum
from types import FunctionType
from PIL import Image
from pathlib import Path

class FormatType(Enum):
    AWAKENING = 0
    FATES = 1
    ECHOES = 2

class InputData:
    darken_torso: bool = False
    original_img: Image = None
    offset: dict = {}
    format_type: FormatType = FormatType.AWAKENING
    data = {}

    def __init__(self, darken_torso: bool, original_img: Image, offset: dict, format_type: FormatType, data: dict = {}) -> None:
        self.darken_torso = darken_torso
        self.original_img = original_img
        self.offset = offset
        self.format_type = format_type
        self.data = data

class FatesInput:
    torso_image: Image = None
    offset: dict = {}
    head_image: Image = None
    shadow: Image = None
    fates_torso: bool = False
    fates_head: bool = False
    darken_torso: bool = False

    def __init__(self, torso, offset, head, shadow, fates_torso=False, fates_head=False, darken_torso=False) -> None:
        self.torso_image = torso
        self.offset = offset
        self.head_image = head
        self.shadow = shadow
        self.fates_torso = fates_torso
        self.fates_head = fates_head
        self.darken_torso = darken_torso
    
class Save:
    folder: str
    name: str

    def __init__(self, folder: str = ".", name: str = "") -> None:
        self.folder = folder
        self.name = name

    def do(self, image, x_idx, y_idx, suffix):
        output_path = Path.cwd()
        output_path = output_path.joinpath("outputs/")
        output_path = output_path.joinpath(self.folder)
        output_path = output_path.joinpath(self.name + str(y_idx) + "-" + str(x_idx) + '-' + suffix + '.png')
        print("Now saving at " + output_path.__str__())
        image.save(output_path)

class TransformType:
    save: Save

    def __init__(self, save: Save) -> None:
        self.save = save

    def GetShadowOffset(self, offset: dict) -> tuple:
        return offset.get("shadow", (0, 0))