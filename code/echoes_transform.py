from PIL import Image
from code.make_functions import MakeMoving, MakeStanding
from code.datatypes import TransformType

class EchoesTransform(TransformType):

    def DoEchoesTransformFullbody(self, original_image, offset, shadow):
        height_bonus = 0
        y = 0
        y_idx = 0
        transparent = (0, 0, 0, 0)

        # The echoes sprite sheets have this weird buffer between normal anims and moving anims
        vertical_buffer = 4

        section_width = (32 * 4)
        section_height = 160 + height_bonus + vertical_buffer

        sprite_height = 32 + height_bonus

        original_image = original_image.crop((2, 2, original_image.size[0] - 2, original_image.size[1] - 2))

        section_spacing = 132

        # Remove transparency. https://stackoverflow.com/questions/35859140/remove-transparency-alpha-from-any-image-using-pil
        # Fuck if I know how this works
        alpha = original_image.convert('RGBA').split()[-1]
        original_image = original_image.convert("RGBA")
        new_img = Image.new("RGBA", original_image.size, transparent)
        new_img.paste(original_image, mask=alpha)
        original_image = new_img

        while y < original_image.height - section_height:
            row_y = y
            sprites = []

            while row_y < y + section_height:
                # The right half of Echoes sheets are for shadow sprites (we don't use)
                print("Now cropping " + str((0, row_y, section_width, row_y + sprite_height)))

                new_img = original_image.crop((0, row_y, section_width, row_y + sprite_height))

                sprites.append(new_img)

                if row_y == y:
                    row_y += vertical_buffer
                row_y += sprite_height

            awakening_format = Image.new("RGBA", (section_width, sprite_height * 6))
            awakening_format.paste(sprites[0], (0, 0, section_width, sprite_height))
            awakening_format.paste(sprites[3], (0, sprite_height, section_width, sprite_height * 2))
            awakening_format.paste(sprites[1], (0, sprite_height * 2, section_width, sprite_height * 3))
            awakening_format.paste(sprites[2], (0, sprite_height * 3, section_width, sprite_height * 4))
            awakening_format.paste(sprites[3], (0, sprite_height * 4, section_width, sprite_height * 5))
            awakening_format.paste(sprites[4], (0, sprite_height * 5, section_width, sprite_height * 6))

            standing_image = MakeStanding(awakening_format, sprite_height, shadow, self.GetShadowOffset(offset))
            self.save.do(standing_image, 0, y_idx, "stand")

            moving_image = MakeMoving(awakening_format, sprite_height, shadow, self.GetShadowOffset(offset))
            self.save.do(moving_image, 0, y_idx, "move")

            # Move y to the bottom of the section, then add spacing
            y += section_height + section_spacing
            y_idx += 1