from PIL import Image
from code.make_functions import MakeMoving, MakeStanding
from code.datatypes import FatesInput, TransformType

# Kind of a weird catch-all for what could be an echoes or fates sheet
# "New style" is basically "head and torso are separate sheets"

class NewStyleTransform(TransformType):
    
    def AnyNeighborsTransparent(self, img, x, y):
        valid_neighbors = []
        w, h = img.size
        if x < w - 1:
            valid_neighbors.append(img.getpixel((x + 1, y))[3] == 0)
        if x > 0:
            valid_neighbors.append(img.getpixel((x - 1, y))[3] == 0)
        if y < h - 1:
            valid_neighbors.append(img.getpixel((x, y + 1))[3] == 0)
        if y > 0:
            valid_neighbors.append(img.getpixel((x, y - 1))[3] == 0)
        return any(valid_neighbors)

    def SplitOffBehind(self, head_images: list, shadow_images: list):
        behind_sprites = []
        
        i = 0
        while i < len(shadow_images):
            shadow = shadow_images[i]
            shadow = shadow.convert("RGBA")
            behind_sprite = Image.new("RGBA", head_images[i].size, (0, 0, 0, 0))

            w, h = shadow.size
            for y in range(h):
                for x in range(w):
                    if shadow.getpixel((x,y))[0] == 102 and shadow.getpixel((x,y))[1] == 102 and shadow.getpixel((x,y))[2] == 102:
                        behind_sprite.putpixel((x,y), head_images[i].getpixel((x,y)))
                        head_images[i].putpixel((x,y), (0, 0, 0, 0))

            behind_sprites.append(behind_sprite)
            i += 1

        return head_images, behind_sprites

    def GetHeadImageAwakeningFormat(self, head_image, fates=False):
        behind_sprites = []
        back_sprite_list = []
        height_bonus = 0
        y = 0
        y_idx = 0

        # The echoes sprite sheets have this weird buffer between normal anims and moving anims
        vertical_buffer = 36

        section_width = (32 * 4)
        section_height = 160 + height_bonus + vertical_buffer

        sprite_height = 32 + height_bonus
        sprite_width = 32

        head_image = head_image.crop((2, 2, head_image.size[0] - 2, head_image.size[1] - 2))

        section_spacing = 132

        if fates:
            section_spacing += 256

        right_side = 128

        sprite_list = []

        while y < head_image.height - section_height:
            row_y = y
            section_sprites = []
            section_shadows = []

            while row_y < y + section_height:
                # The right half of Echoes/Fates sheets are for shadow sprites (we use just for fates)
                print("Now cropping " + str((0, row_y, section_width, row_y + sprite_height)))

                i = 0
                while i < 4:
                    new_img = head_image.crop((i * sprite_width, row_y, (i + 1) * sprite_width, row_y + sprite_height))
                    section_sprites.append(new_img)

                    shadow_img = head_image.crop(((i * sprite_width) + right_side, row_y, ((i + 1) * sprite_width) + right_side, row_y + sprite_height))
                    section_shadows.append(shadow_img)
                    i += 1

                if row_y == y:
                    row_y += vertical_buffer
                row_y += sprite_height

            if fates:
                section_sprites, behind_sprites = self.SplitOffBehind(section_sprites, section_shadows)

            sprite_list.extend(section_sprites)
            back_sprite_list.extend(behind_sprites)

            # Move y to the bottom of the section, then add spacing
            y += section_height + section_spacing
            y_idx += 1

        return section_sprites, behind_sprites

    def GetOffset(self, offset: dict, name: str, x_idx: int, y_idx: int, w: int, h: int):
        return (offset.get(name, (0, 0))[0] + (x_idx * w), \
                    offset.get(name, (0, 0))[1] + (y_idx * w), \
                    offset.get(name, (0, 0))[0] + ((x_idx + 1) * w), \
                    offset.get(name, (0, 0))[1] + ((y_idx + 1) * h))

    def PasteHeads(self, img, head_list, spr_w, spr_h, offset):
        i = 0
        while i < len(head_list):
            head_list[i] = head_list[i].convert("RGBA")
            i += 1

        if len(head_list) > 19:
            img.paste(head_list[0], self.GetOffset(offset, "passive1", 0, 0, spr_w, spr_h), head_list[0])
            img.paste(head_list[1], self.GetOffset(offset, "passive2", 1, 0, spr_w, spr_h), head_list[1])
            img.paste(head_list[2], self.GetOffset(offset, "passive3", 2, 0, spr_w, spr_h), head_list[2])
            img.paste(head_list[3], self.GetOffset(offset, "passive4", 3, 0, spr_w, spr_h), head_list[3])

            img.paste(head_list[0], self.GetOffset(offset, "passive1", 0, 1, spr_w, spr_h), head_list[0])
            img.paste(head_list[1], self.GetOffset(offset, "passive2", 1, 1, spr_w, spr_h), head_list[1])
            img.paste(head_list[2], self.GetOffset(offset, "passive3", 2, 1, spr_w, spr_h), head_list[2])
            img.paste(head_list[3], self.GetOffset(offset, "passive4", 3, 1, spr_w, spr_h), head_list[3])

            img.paste(head_list[4], self.GetOffset(offset, "left1", 0, 2, spr_w, spr_h), head_list[4])
            img.paste(head_list[5], self.GetOffset(offset, "left2", 1, 2, spr_w, spr_h), head_list[5])
            img.paste(head_list[6], self.GetOffset(offset, "left3", 2, 2, spr_w, spr_h), head_list[6])
            img.paste(head_list[7], self.GetOffset(offset, "left4", 3, 2, spr_w, spr_h), head_list[7])

            img.paste(head_list[8], self.GetOffset(offset, "right1", 0, 3, spr_w, spr_h), head_list[8])
            img.paste(head_list[9], self.GetOffset(offset, "right2", 1, 3, spr_w, spr_h), head_list[9])
            img.paste(head_list[10], self.GetOffset(offset, "right3", 2, 3, spr_w, spr_h), head_list[10])
            img.paste(head_list[11], self.GetOffset(offset, "right4", 3, 3, spr_w, spr_h), head_list[11])

            img.paste(head_list[12], self.GetOffset(offset, "down1", 0, 4, spr_w, spr_h), head_list[12])
            img.paste(head_list[13], self.GetOffset(offset, "down2", 1, 4, spr_w, spr_h), head_list[13])
            img.paste(head_list[14], self.GetOffset(offset, "down3", 2, 4, spr_w, spr_h), head_list[14])
            img.paste(head_list[15], self.GetOffset(offset, "down4", 3, 4, spr_w, spr_h), head_list[15])

            img.paste(head_list[16], self.GetOffset(offset, "up1", 0, 5, spr_w, spr_h), head_list[16])
            img.paste(head_list[17], self.GetOffset(offset, "up2", 1, 5, spr_w, spr_h), head_list[17])
            img.paste(head_list[18], self.GetOffset(offset, "up3", 2, 5, spr_w, spr_h), head_list[18])
            img.paste(head_list[19], self.GetOffset(offset, "up4", 3, 5, spr_w, spr_h), head_list[19])
        else:
            print("Mismatch between torso and head sheet length. Continuing headless...")

        return img

    def DarkenImage(self, img):
        w, h = img.size
        darken_multiplier = 0.4
        for y in range(h):
            for x in range(w):
                if img.getpixel((x, y))[3] != 0 and self.AnyNeighborsTransparent(img, x, y):
                    pxl = img.getpixel((x, y))
                    new_pxl = (int(pxl[0] * darken_multiplier), int(pxl[1] * darken_multiplier), int(pxl[2] * darken_multiplier))
                    img.putpixel((x, y), new_pxl)
        return img

    def DoNewStyleTransformSplit(self, input: FatesInput):
        torso = input.torso_image
        offset = input.offset
        head = input.head_image
        shadow = input.shadow
        fates = input.fates_torso
        darken_torso = input.darken_torso
        fates_head = input.fates_head
        head_sprites, behind_sprites = self.GetHeadImageAwakeningFormat(head, fates_head)

        transparent = (0, 0, 0, 0)

        height_bonus = 0
        y = 0
        y_idx = 0

        # The echoes sprite sheets have this weird buffer between normal anims and moving anims
        vertical_buffer = 4

        spr_w = 32
        section_width = (spr_w * 4)
        section_height = 160 + height_bonus + vertical_buffer

        sprite_height = 32 + height_bonus

        torso = torso.crop((2, 2, torso.size[0] - 2, torso.size[1] - 2))

        section_spacing = 132

        if fates:
            section_spacing += 256

        while y < torso.height - section_height:
            row_y = y
            sprites = []

            while row_y < y + section_height:
                # The right half of Echoes sheets are for shadow sprites (we don't use)
                print("Now cropping " + str((0, row_y, section_width, row_y + sprite_height)))

                new_img = torso.crop((0, row_y, section_width, row_y + sprite_height))

                new_img = new_img.convert("RGBA")
                if fates and darken_torso:
                    new_img = self.DarkenImage(new_img)

                sprites.append(new_img)

                if row_y == y:
                    row_y += vertical_buffer
                row_y += sprite_height

            awakening_format = Image.new("RGBA", (section_width, sprite_height * 6))

            if fates:
                awakening_format = self.PasteHeads(awakening_format, behind_sprites, spr_w, sprite_height, offset)
            
            i = 0
            while i < len(sprites):
                sprites[i] = sprites[i].convert("RGBA")
                i += 1

            awakening_format.paste(sprites[0], (0, 0, section_width, sprite_height), sprites[0])
            awakening_format.paste(sprites[0], (0, sprite_height, section_width, sprite_height * 2), sprites[0])
            awakening_format.paste(sprites[1], (0, sprite_height * 2, section_width, sprite_height * 3), sprites[1])
            awakening_format.paste(sprites[2], (0, sprite_height * 3, section_width, sprite_height * 4), sprites[2])
            awakening_format.paste(sprites[3], (0, sprite_height * 4, section_width, sprite_height * 5), sprites[3])
            awakening_format.paste(sprites[4], (0, sprite_height * 5, section_width, sprite_height * 6), sprites[4])

            awakening_format = self.PasteHeads(awakening_format, head_sprites, spr_w, sprite_height, offset)

            # Remove transparency. https://stackoverflow.com/questions/35859140/remove-transparency-alpha-from-any-image-using-pil
            # Fuck if I know how this works
            alpha = awakening_format.convert('RGBA').split()[-1]
            awakening_format = awakening_format.convert("RGBA")
            new_img = Image.new("RGBA", awakening_format.size, transparent)
            new_img.paste(awakening_format, mask=alpha)
            awakening_format = new_img

            standing_image = MakeStanding(awakening_format, sprite_height, shadow, self.GetShadowOffset(offset))
            self.save.do(standing_image, 0, y_idx, "stand")

            moving_image = MakeMoving(awakening_format, sprite_height, shadow, self.GetShadowOffset(offset))
            self.save.do(moving_image, 0, y_idx, "move")

            # Move y to the bottom of the section, then add spacing
            y += section_height + section_spacing
            y_idx += 1