from PIL import Image

bg_color = (128, 160, 128)

def GetBGColor() -> tuple:
    return bg_color

def FillWithBGColor(img):
    img.paste(bg_color, (0, 0, img.size[0], img.size[1]))
    return img

def MakeMoving(img, sprite_height: int, shadow, shadow_offset: tuple):
    bottom = sprite_height - 1
    moving_image = Image.new("RGBA", (192, 160))
    moving_image = FillWithBGColor(moving_image)

    starting_height = sprite_height * 2

    left1 = img.crop((0, starting_height, 32, starting_height + bottom))
    left2 = img.crop((32, starting_height, 64, starting_height + bottom))
    left3 = img.crop((64, starting_height, 96, starting_height + bottom))
    left4 = img.crop((96, starting_height, 128, starting_height + bottom))

    starting_height += sprite_height

    right1 = img.crop((0, starting_height, 32, starting_height + bottom))
    right2 = img.crop((32, starting_height, 64, starting_height + bottom))
    right3 = img.crop((64, starting_height, 96, starting_height + bottom))
    right4 = img.crop((96, starting_height, 128, starting_height + bottom))

    starting_height += sprite_height

    down1 = img.crop((0, starting_height, 32, starting_height + bottom))
    down2 = img.crop((32, starting_height, 64, starting_height + bottom))
    down3 = img.crop((64, starting_height, 96, starting_height + bottom))
    down4 = img.crop((96, starting_height, 128, starting_height + bottom))

    starting_height += sprite_height

    up1 = img.crop((0, starting_height, 32, starting_height + bottom))
    up2 = img.crop((32, starting_height, 64, starting_height + bottom))
    up3 = img.crop((64, starting_height, 96, starting_height + bottom))
    up4 = img.crop((96, starting_height, 128, starting_height + bottom))

    horiztonal_spacing = 48
    vertical_spacing = 40
    top_buffer = 10
    left_buffer = 8

    left_shadow = left_buffer + shadow_offset[0] # Left buffer + shadow x spacing

    moving_image.paste(shadow, (left_shadow, top_buffer + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing, top_buffer + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing * 2, top_buffer + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing * 3, top_buffer + shadow_offset[1]))

    moving_image.paste(down1, (left_buffer, top_buffer), down1)
    moving_image.paste(down2, (left_buffer + horiztonal_spacing, top_buffer), down2)
    moving_image.paste(down3, (left_buffer + horiztonal_spacing * 2, top_buffer), down3)
    moving_image.paste(down4, (left_buffer + horiztonal_spacing * 3, top_buffer), down4)

    moving_image.paste(shadow, (left_shadow, top_buffer + vertical_spacing + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing, top_buffer + vertical_spacing + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing * 2, top_buffer + vertical_spacing + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing * 3, top_buffer + vertical_spacing + shadow_offset[1]))

    moving_image.paste(left1, (left_buffer, top_buffer + vertical_spacing), left1)
    moving_image.paste(left2, (left_buffer + horiztonal_spacing, top_buffer + vertical_spacing), left2)
    moving_image.paste(left3, (left_buffer + horiztonal_spacing * 2, top_buffer + vertical_spacing), left3)
    moving_image.paste(left4, (left_buffer + horiztonal_spacing * 3, top_buffer + vertical_spacing), left4)

    moving_image.paste(shadow, (left_shadow, top_buffer + vertical_spacing * 2 + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing, top_buffer + vertical_spacing * 2 + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing * 2, top_buffer + vertical_spacing * 2 + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing * 3, top_buffer + vertical_spacing * 2 + shadow_offset[1]))

    moving_image.paste(right1, (left_buffer, top_buffer + vertical_spacing * 2), right1)
    moving_image.paste(right2, (left_buffer + horiztonal_spacing, top_buffer + vertical_spacing * 2), right2)
    moving_image.paste(right3, (left_buffer + horiztonal_spacing * 2, top_buffer + vertical_spacing * 2), right3)
    moving_image.paste(right4, (left_buffer + horiztonal_spacing * 3, top_buffer + vertical_spacing * 2), right4)

    moving_image.paste(shadow, (left_shadow, top_buffer + vertical_spacing * 3 + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing, top_buffer + vertical_spacing * 3 + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing * 2, top_buffer + vertical_spacing * 3 + shadow_offset[1]))
    moving_image.paste(shadow, (left_shadow + horiztonal_spacing * 3, top_buffer + vertical_spacing * 3 + shadow_offset[1]))

    moving_image.paste(up1, (left_buffer, top_buffer + vertical_spacing * 3), up1)
    moving_image.paste(up2, (left_buffer + horiztonal_spacing, top_buffer + vertical_spacing * 3), up2)
    moving_image.paste(up3, (left_buffer + horiztonal_spacing * 2, top_buffer + vertical_spacing * 3), up3)
    moving_image.paste(up4, (left_buffer + horiztonal_spacing * 3, top_buffer + vertical_spacing * 3), up4)

    return moving_image

# All 3DS sprites have width 32
def MakeStanding(img, sprite_height: int, shadow, shadow_offset: tuple):
    bottom = sprite_height - 1

    standing_image = Image.new("RGBA", (192, 144))

    standing_image = FillWithBGColor(standing_image)

    passive1 = img.crop((0, 0, 32, bottom))
    passive2 = img.crop((32, 0, 64, bottom))
    passive3 = img.crop((64, 0, 96, bottom))

    selected1 = img.crop((0, sprite_height, 32, sprite_height + bottom))
    selected2 = img.crop((32, sprite_height, 64, sprite_height + bottom))
    selected3 = img.crop((64, sprite_height, 96, sprite_height + bottom))

    left = 16 # Dead space on the left of an LT image
    new_spacing = 64 # Space between sprite frames
    top_buffer = 12 # Dead space on the top of an LT image

    # Shadow
    standing_image.paste(shadow, (left + shadow_offset[0], top_buffer + shadow_offset[1]))
    standing_image.paste(shadow, (left + new_spacing + shadow_offset[0], top_buffer + shadow_offset[1]))
    standing_image.paste(shadow, (left + (new_spacing* 2) + shadow_offset[0], top_buffer + shadow_offset[1]))

    standing_image.paste(passive1, (left, top_buffer), passive1)
    standing_image.paste(passive2, (left + new_spacing, top_buffer), passive2)
    standing_image.paste(passive3, (left + (new_spacing* 2), top_buffer), passive3)

    three_tiles = 96 # This + top buffer = Start of third row (selected frames)

    standing_image.paste(shadow, (left + shadow_offset[0], top_buffer + three_tiles + shadow_offset[1]))
    standing_image.paste(shadow, (left + new_spacing + shadow_offset[0], top_buffer + three_tiles + shadow_offset[1]))
    standing_image.paste(shadow, (left + (new_spacing* 2) + shadow_offset[0], top_buffer + three_tiles + shadow_offset[1]))

    standing_image.paste(selected1, (left, top_buffer + three_tiles), selected1)
    standing_image.paste(selected2, (left + new_spacing, top_buffer + three_tiles), selected2)
    standing_image.paste(selected3, (left + (new_spacing* 2), top_buffer + three_tiles), selected3)

    return standing_image