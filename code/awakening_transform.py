import numpy as np
from PIL import Image
from code.make_functions import GetBGColor, MakeMoving, MakeStanding
from code.datatypes import TransformType

class AwakeningTransform(TransformType):

    def DoAwakeningTransform(self, original_image, shadow, offset):
        is_flier = input("Is your sprite sheet of a flier? Fliers must be inputted in a sheet of only fliers.\nType Yes if yes, or anything else if no: ")
        height_bonus = 0
        if is_flier.upper() == "YES":
            height_bonus = 4

        x = 0
        y = 0
        x_idx = 0
        y_idx = 0

        section_width = (32 * 4)
        section_height = 192 + height_bonus

        sprite_height = 32

        # The second part (32 * 5) is to account for the five sprites we leave out (moving diagnol)
        skipped_sprites = (sprite_height * 4) - height_bonus
        section_spacing = 192 + skipped_sprites

        while y < original_image.height - section_height:
            while x < original_image.width - section_width + 1:
                print("Now cropping " + str((x, y, x + section_width, y + section_height)))

                new_img = original_image.crop((x, y, x + section_width, y + section_height))
                
                new_img = new_img.convert('RGBA')

                # Shamelessly stolen from https://stackoverflow.com/questions/3752476/python-pil-replace-a-single-rgba-color
                data = np.array(new_img)
                red, green, blue, alpha = data.T

                pink_areas = (red == 255) & (blue == 255) & (green == 0)
                data[..., :-1][pink_areas.T] = GetBGColor()

                new_img = Image.fromarray(data)

                standing_image = MakeStanding(new_img, sprite_height + height_bonus, shadow, self.GetShadowOffset(offset))
                self.save.do(standing_image, x_idx, y_idx, "stand")

                moving_image = MakeMoving(new_img, sprite_height + height_bonus, shadow, self.GetShadowOffset(offset))
                self.save.do(moving_image, x_idx, y_idx, "move")

                x += section_width
                x_idx += 1

            y += section_height + section_spacing
            y_idx += 1
            x = 0
            x_idx = 0
